package webpage.pageObjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://localhost:4200/checkout/")
public class CheckoutPage extends PageObject {
    public static final Target NAME_SURNAME_FIELD = Target
            .the("Name Surname field")
            .locatedBy("//*[@id=\"shipName\"]");

    public static final Target PHONE_NUMBER_FIELD = Target
            .the("Phone Number field")
            .locatedBy("//*[@id=\"phone\"]");

    public static final Target COUNTRY_FIELD = Target
            .the("Country field")
            .locatedBy("//*[@id=\"country\"]");

    public static final Target CITY_FIELD = Target
            .the("City field")
            .locatedBy("//*[@id=\"city\"]");

    public static final Target STATE_FIELD = Target
            .the("State field")
            .locatedBy("//*[@id=\"state\"]");

    public static final Target ZIP_FIELD = Target
            .the("ZIP field")
            .locatedBy("//*[@id=\"zip\"]");

    public static final Target ADDRESS_FIELD = Target
            .the("Address field")
            .locatedBy("//*[@id=\"shipAddress\"]");

    public static final Target CARD_NUMBER_FIELD = Target
            .the("Card Number field")
            .locatedBy("//*[@id=\"cardNo\"]");

    public static final Target CARD_OWNER_FIELD = Target
            .the("Card Owner field")
            .locatedBy("//*[@id=\"cardOwner\"]");

    public static final Target MONTH_DROP_DOWN = Target
            .the("Month Drop Down")
            .locatedBy("//*[@id=\"month\"]");

    public static final Target MONTH_DROP_DOWN_OPTION = Target
            .the("Month Drop Down Option")
            .locatedBy("/html/body/app-root/app-checkout/div/div/div[1]/app-payment/div/form/div[1]/div[2]/div[1]/div/div[1]/div/select/option[1]");

    public static final Target Year_DROP_DOWN = Target
            .the("Year Drop Down")
            .locatedBy("//*[@id=\"year\"]");

    public static final Target YEAR_DROP_DOWN_OPTION = Target
            .the("Year Drop Down Option")
            .locatedBy("/html/body/app-root/app-checkout/div/div/div[1]/app-payment/div/form/div[1]/div[2]/div[1]/div/div[2]/div/select/option[1]");

    public static final Target CCV_FIELD = Target
            .the("CCV field")
            .locatedBy("//*[@id=\"cardCCV\"]");

    public static final Target TERMS_CHECKBOX = Target
            .the("Terms Checkbox")
            .locatedBy("//*[@id=\"terms\"]");

    public static final Target PURCHASE_BUTTON = Target
            .the("Purchase Button")
            .locatedBy("/html/body/app-root/app-checkout/div/div/div[3]/app-summary/div/div[5]/div[2]/button");

    public static final Target CONFIRM_BUTTON = Target
            .the("Cart item")
            .locatedBy("/html/body/ngb-modal-window/div/div/app-bank-accept/div[3]/button");

    public static final Target PI_CONTINUE_BUTTON = Target
            .the("Personal Information Continue Button")
            .locatedBy("/html/body/app-root/app-checkout/div/div/div[1]/app-personal/div/div/form/div[2]/div[2]/div/div[2]/button");

    public static final Target SHIPPING_CONTINUE_BUTTON = Target
            .the("Shipping Continue Button")
            .locatedBy("/html/body/app-root/app-checkout/div/div/div[1]/app-shipping/div/div/form/div[2]/div[2]/div/div[2]/button");

    public static final Target PAYMENT_CONTINUE_BUTTON = Target
            .the("Payment Continue Button")
            .locatedBy("/html/body/app-root/app-checkout/div/div/div[1]/app-payment/div/form/div[2]/div[2]/div/div[2]/button");


    public static final Target ORDERS_BUTTON = Target
            .the("Cart item")
            .locatedBy("/html/body/app-root/app-success/div/div[2]/div/a[2]");
}