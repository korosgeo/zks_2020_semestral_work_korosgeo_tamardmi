package webpage.pageObjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://localhost:4200/login")
public class ProductDetailPage extends PageObject {
    public static final Target AMOUNT_FIELD = Target
            .the("Amount field")
            .locatedBy("div.row:nth-child(2) > input:nth-child(1)");

    public static final Target ADD_TO_CART_BUTTON = Target
            .the("Add to cart button")
            .locatedBy("/html/body/app-root/app-product-detail/div/div/div[2]/div/div[2]/div/button");

    public static final Target PRODUCT_NAME = Target
            .the("Product name")
            .locatedBy(".name");
}