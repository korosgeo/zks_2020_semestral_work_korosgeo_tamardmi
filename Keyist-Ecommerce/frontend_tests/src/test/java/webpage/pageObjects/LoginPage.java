package webpage.pageObjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://localhost:4200/login")
public class LoginPage extends PageObject {
    public static final Target LOGIN_PAGE_INDICATOR = Target
            .the("Login page indicator")
            .locatedBy("/html/body/app-root/app-signin/div/div[1]/div[2]/p");

    public static final Target LOGIN_PAGE_LINK = Target
            .the("Login page link")
            .locatedBy(".login");

    public static final Target EMAIL_FIELD = Target
            .the("Email field")
            .locatedBy("#email");

    public static final Target PASSWORD_FIELD = Target
            .the("Password field")
            .locatedBy("#password");

    public static final Target LOGIN_SUBMIT_BUTTON = Target
            .the("Login submit button")
            .locatedBy("/html/body/app-root/app-signin/div/div[3]/div/form/div[4]/div/button");
}
