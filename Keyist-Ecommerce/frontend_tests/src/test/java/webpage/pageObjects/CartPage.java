package webpage.pageObjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://localhost:4200/cart")
public class CartPage extends PageObject {
    public static final Target CART_CHECKOUT_BUTTON = Target
            .the("Cart checkout button")
            .locatedBy("/html/body/app-root/app-cart/div[1]/div/div[3]/div[4]/div/div/button");

    public static final Target CART_ITEM = Target
            .the("Cart item")
            .locatedBy("/html/body/app-root/app-cart/div[1]/div/div[1]/div[2]/div[2]/span");

    public static final Target REMOVE_BUTTON = Target
            .the("Remove Button")
            .locatedBy("/html/body/app-root/app-cart/div[1]/div/div[1]/div[2]/div[5]/span");
}