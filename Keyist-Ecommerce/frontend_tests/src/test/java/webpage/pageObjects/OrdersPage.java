package webpage.pageObjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://localhost:4200/cart")
public class OrdersPage extends PageObject {
    public static final Target LAST_ORDER_STATUS = Target
            .the("Last Order Status")
            .locatedBy("/html/body/app-root/app-account/div/div/div[2]/app-list-orders/div/div[2]/div[1]/div[1]/div/div[3]/p/span");

    public static final Target LAST_ORDER_NUMBER = Target
            .the("Last Order Number")
            .locatedBy("/html/body/app-root/app-account/div/div/div[2]/app-list-orders/div/div[2]/div[1]/div[1]/div/div[1]/p/span");

    public static final Target LAST_ORDERED_PRODUCT_NAME = Target
            .the("Last Order Number")
            .locatedBy("/html/body/app-root/app-account/div/div/div[2]/app-list-orders/div/div[2]/div[1]/div[2]/div[2]/div/div/div/div[2]/div[1]/div/p");

    public static final Target LAST_ORDERED_PRODUCT_AMOUNT = Target
            .the("Last Order Number")
            .locatedBy("/html/body/app-root/app-account/div/div/div[2]/app-list-orders/div/div[2]/div[1]/div[2]/div[2]/div/div/div/div[2]/div[2]/div[1]/p/span");

}