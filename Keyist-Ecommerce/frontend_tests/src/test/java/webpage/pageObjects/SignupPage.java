package webpage.pageObjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://localhost:4200/signup")
public class SignupPage extends PageObject {
    public static final Target SIGNUP_PAGE_INDICATOR = Target
            .the("Sign up page indicator")
            .locatedBy(".col-md-8 > p:nth-child(1)");

    public static final Target SIGNUP_PAGE_BUTTON = Target
            .the("Sing up page button")
            .locatedBy(".register");

    public static final Target EMAIL_FIELD = Target
            .the("Email field")
            .locatedBy("#email");

    public static final Target NEW_PASSWORD_FIELD = Target
            .the("Password field")
            .locatedBy("#newPassword");

    public static final Target NEW_PASSWORD_CONFIRM_FIELD = Target
            .the("Password field")
            .locatedBy("#newPasswordConfirm");

    public static final Target REGISTER_SUBMIT_BUTTON = Target
            .the("Register submit button")
            .locatedBy("/html/body/app-root/app-signup/div/div[3]/div/form/div[3]/div/button");
}