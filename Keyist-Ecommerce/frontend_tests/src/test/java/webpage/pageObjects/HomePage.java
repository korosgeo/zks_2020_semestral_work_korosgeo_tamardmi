package webpage.pageObjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://localhost:4200/")
public class HomePage extends PageObject {
    public static final Target HOME_PAGE_INDICATOR = Target
            .the("Home page indicator")
            .locatedBy("/html/body/app-root/app-header/div/nav/div[1]/div[1]/div/a");

    public static final Target MOST_SELLING_PRODUCT = Target
            .the("Most selling product")
            .locatedBy("/html/body/app-root/app-home/div/app-most-selling/div/div[3]/div[1]");

    public static final Target ACCOUNT_DROP_DOWN = Target
            .the("Account drop down")
            .locatedBy("//*[@id=\"dropdownAccount\"]");

    public static final Target CART_DROP_DOWN = Target
            .the("Cart drop down")
            .locatedBy("//*[@id=\"dropdownCart nav-link\"]");

    public static final Target CART_DROP_DOWN_CHECKOUT_BUTTON = Target
            .the("Cart drop down checkout button")
            .locatedBy("/html/body/app-root/app-header/div/nav/div[3]/div[2]/div/div[2]/div/div[2]/div/div[2]/button");

    public static final Target CART_DROP_DOWN_GO_TO_CART = Target
            .the("Cart drop down go to cart button")
            .locatedBy("/html/body/app-root/app-header/div/nav/div[3]/div[2]/div/div[2]/div/div[2]/div/div[1]/a");

    public static final Target USER_WARING = Target
            .the("User warning")
            .locatedBy(".user-warning");

}