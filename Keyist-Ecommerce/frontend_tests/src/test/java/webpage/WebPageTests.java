package webpage;

import cz.cvut.fel.still.sqa.seleniumStarterPack.config.DriverFactory;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import org.junit.*;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import webpage.tasks.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.Matchers.is;
import static webpage.pageObjects.HomePage.ACCOUNT_DROP_DOWN;
import static webpage.questions.AccountStatusQuestion.theAccountIsActive;
import static webpage.questions.CartQuestion.theCartItemName;
import static webpage.questions.HomePageQuestion.theHomePageIndicator;
import static webpage.questions.LoginPageQuestion.theLoginPageIndicator;
import static webpage.questions.MostSellingProductQuestion.theMostSellingProductName;
import static webpage.questions.OrderQuestion.theOrderInformation;

@RunWith(SerenityRunner.class)
public class WebPageTests {

    Actor james = Actor.named("James");

    private WebDriver theBrowser;
    String newUserEmail = "user1@mail.com";
    String newUserPassword = "123456";

    String inactiveUserEmail = "inactive_user@mail.com";
    String inactiveUserPassword = "123456";

    String activeUserEmail = "active_user@mail.com";
    String activeUserPassword = "123456";

    Map<String, String> formsInfo = new HashMap<String, String>() {{
        put("nameSurname", "Donald Trump");
        put("phoneNumber", "77777777777");
        put("country", "USA");
        put("city", "Washington");
        put("state", "Washington DC");
        put("zip", "20500");
        put("address", "1600 Pennsylvania Avenue NW");
        put("cardNumber", "9999999999999");
        put("cardOwner", "DONALD TRUMP");
        put("ccv", "999");
    }};

    @Before
    public void before() throws IOException {
        theBrowser = new DriverFactory().getDriver();
        givenThat(james).can(BrowseTheWeb.with(theBrowser));
    }

    @Test
    public void should_be_able_to_see_home_page() {
        givenThat(james).wasAbleTo(StartWith.mainPage());
        then(james).should(seeThat(theHomePageIndicator(), is("nav-link active")));
    }

    @Test
    public void should_be_able_to_see_item_details() {
        String mostSellingProductName = "Owl";
        givenThat(james).wasAbleTo(StartWith.mainPage());
        when(james).attemptsTo(SeeItemDetails.called());
        then(james).should(seeThat(theMostSellingProductName(), is(mostSellingProductName)));
    }

    @Test
    public void should_not_be_able_to_add_items_to_cart_without_being_logged_in() {
        String expectedUrl = "http://localhost:4200/login";

        givenThat(james).wasAbleTo(StartWith.mainPage());
        when(james).attemptsTo(AddMostSellingItemToTheCart.called());
        // Check URL
        Assert.assertEquals(expectedUrl, theBrowser.getCurrentUrl());
        then(james).should(seeThat(theLoginPageIndicator(), is("LOGIN")));
    }

    @Test
    public void should_be_able_to_log_in_after_account_creation() {
        givenThat(james).wasAbleTo(StartWith.mainPage());
        when(james).attemptsTo(CreateNewAccount.called(newUserEmail, newUserPassword));
        when(james).attemptsTo(Login.called(newUserEmail, newUserPassword));
        then(james).should(seeThat(theHomePageIndicator(), is("nav-link active")));
    }

    @Test
    public void should_be_able_to_add_items_to_cart_when_logged_in() {

        givenThat(james).wasAbleTo(StartWith.mainPage());
        when(james).attemptsTo(Login.called(inactiveUserEmail, inactiveUserPassword));
//        if (theBrowser.findElements(new By.ByXPath(ACCOUNT_DROP_DOWN.getCssOrXPathSelector())).size() == 0) {
//            when(james).attemptsTo(Login.called(inactiveUserEmail, inactiveUserPassword));
//        }
        when(james).attemptsTo(AddMostSellingItemToTheCart.called());
        then(james).should(seeThat(theCartItemName(), is("Owl")));
    }

    @Test
    public void should_not_be_able_to_checkout_without_account_activation() {
        givenThat(james).wasAbleTo(StartWith.mainPage());
        when(james).attemptsTo(Login.called(inactiveUserEmail, inactiveUserPassword));
        when(james).attemptsTo(AddMostSellingItemToTheCart.called());
        when(james).attemptsTo(ViewCart.called());
        when(james).attemptsTo(ClickCheckoutButton.called());
        then(james).should(seeThat(theAccountIsActive(theBrowser), is(false)));
    }

    @Test
    public void should_be_able_to_complete_the_order() {
        ArrayList<String> orderInfo = new ArrayList<String>();
        orderInfo.add("#1");
        orderInfo.add("Preparing");
        orderInfo.add("Owl");
        orderInfo.add("1");

        givenThat(james).wasAbleTo(StartWith.mainPage());
        when(james).attemptsTo(Login.called(activeUserEmail, activeUserPassword));
        when(james).attemptsTo(AddMostSellingItemToTheCart.called());
        when(james).attemptsTo(ViewCart.called());
        when(james).attemptsTo(ClickCheckoutButton.called());
        when(james).attemptsTo(FillOrderForms.called(formsInfo));
        when(james).attemptsTo(ConfirmOrder.called());
        when(james).attemptsTo(ViewOrders.called());
        then(james).should(seeThat(theOrderInformation(), is(orderInfo)));
    }

    @After
    public void closeBrowser() {
        theBrowser.close();
    }
}
