package webpage.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import static webpage.pageObjects.LoginPage.LOGIN_PAGE_INDICATOR;

public class LoginPageQuestion implements Question<String> {

    public static Question<String> theLoginPageIndicator() { return new LoginPageQuestion(); }

    @Override
    public String answeredBy(Actor actor) { return Text.of(LOGIN_PAGE_INDICATOR).viewedBy(actor).asString(); }
}