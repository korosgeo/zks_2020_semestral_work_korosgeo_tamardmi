package webpage.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AccountStatusQuestion implements Question<Boolean> {

    private final WebDriver theBrowser;

    public static Question<Boolean> theAccountIsActive(WebDriver theBrowser) { return new AccountStatusQuestion(theBrowser); }

    public AccountStatusQuestion(WebDriver theBrowser) {
        this.theBrowser = theBrowser;
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        try
        {
            String expectedAlertText = "Your account is inactive. You must activate your account in order to purchase.\n" +
                    "Please check your email.";
            Alert alert = theBrowser.switchTo().alert();
            Boolean retValue = !alert.getText().equals(expectedAlertText);
            alert.accept();
            return retValue;
        }
        catch (NoAlertPresentException Ex)
        {
            return true;
        }
    }
}