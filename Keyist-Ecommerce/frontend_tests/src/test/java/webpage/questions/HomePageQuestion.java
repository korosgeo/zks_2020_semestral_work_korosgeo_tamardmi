package webpage.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Attribute;

import static webpage.pageObjects.HomePage.HOME_PAGE_INDICATOR;

public class HomePageQuestion implements Question<String> {

    public static Question<String> theHomePageIndicator() { return new HomePageQuestion(); }

    @Override
    public String answeredBy(Actor actor) { return Attribute.of(HOME_PAGE_INDICATOR).named("class").viewedBy(actor).asString(); }
}