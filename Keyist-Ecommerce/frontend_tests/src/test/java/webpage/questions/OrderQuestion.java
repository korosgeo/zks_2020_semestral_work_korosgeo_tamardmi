package webpage.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.ArrayList;

import static webpage.pageObjects.OrdersPage.*;

public class OrderQuestion implements Question<ArrayList<String>> {

    public static Question<ArrayList<String>> theOrderInformation() { return new OrderQuestion(); }

    @Override
    public ArrayList<String> answeredBy(Actor actor) {
        ArrayList<String> orderInfo = new ArrayList<String>();
        orderInfo.add(Text.of(LAST_ORDER_NUMBER).viewedBy(actor).asString());
        orderInfo.add(Text.of(LAST_ORDER_STATUS).viewedBy(actor).asString());
        orderInfo.add(Text.of(LAST_ORDERED_PRODUCT_NAME).viewedBy(actor).asString());
        orderInfo.add(Text.of(LAST_ORDERED_PRODUCT_AMOUNT).viewedBy(actor).asString());

        return orderInfo;
    }
}