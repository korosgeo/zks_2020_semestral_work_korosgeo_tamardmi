package webpage.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import static webpage.pageObjects.ProductDetailPage.PRODUCT_NAME;

public class MostSellingProductQuestion implements Question<String> {
    public static Question<String> theMostSellingProductName() {
        return new MostSellingProductQuestion();
    }

    @Override
    public String answeredBy(Actor actor) {
        return Text.of(PRODUCT_NAME).viewedBy(actor).asString();
    }
}
