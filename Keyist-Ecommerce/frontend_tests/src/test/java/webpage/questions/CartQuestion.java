package webpage.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import static webpage.pageObjects.CartPage.CART_ITEM;

public class CartQuestion implements Question<String> {

    public static Question<String> theCartItemName() { return new CartQuestion(); }

    @Override
    public String answeredBy(Actor actor) { return Text.of(CART_ITEM).viewedBy(actor).asString(); }
}