package webpage.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.*;
import net.thucydides.core.annotations.Step;

import static webpage.pageObjects.ProductDetailPage.ADD_TO_CART_BUTTON;

public class AddMostSellingItemToTheCart implements Task {

    public AddMostSellingItemToTheCart() {
    }

    public static AddMostSellingItemToTheCart called() {
        return Instrumented.instanceOf(AddMostSellingItemToTheCart.class).newInstance();
    }

    @Override
    @Step("{0} add most selling item to the cart")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                SeeItemDetails.called(),
                Click.on(ADD_TO_CART_BUTTON)
        );
    }
}
