package webpage.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.*;
import net.serenitybdd.screenplay.waits.WaitUntil;
import net.thucydides.core.annotations.Step;

import static webpage.pageObjects.HomePage.CART_DROP_DOWN;
import static webpage.pageObjects.HomePage.CART_DROP_DOWN_GO_TO_CART;

public class ViewCart implements Task {

    public ViewCart() {
    }

    public static ViewCart called() {
        return Instrumented.instanceOf(ViewCart.class).newInstance();
    }

    @Override
    @Step("{0} view cart")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(CART_DROP_DOWN),
                WaitUntil.angularRequestsHaveFinished(),
                Click.on(CART_DROP_DOWN_GO_TO_CART)
        );
    }
}