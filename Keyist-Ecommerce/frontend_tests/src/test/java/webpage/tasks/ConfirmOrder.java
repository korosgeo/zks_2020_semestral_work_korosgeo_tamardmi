package webpage.tasks;

import net.bytebuddy.description.modifier.Ownership;
import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.*;
import net.thucydides.core.annotations.Step;

import static webpage.pageObjects.CheckoutPage.*;

public class ConfirmOrder implements Task {

    public ConfirmOrder() {
    }

    public static ConfirmOrder called() {
        return Instrumented.instanceOf(ConfirmOrder.class).withProperties();
    }

    @Override
    @Step("{0} confirm order")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(TERMS_CHECKBOX),
                Click.on(PURCHASE_BUTTON),
                Click.on(CONFIRM_BUTTON)
        );
    }
}

