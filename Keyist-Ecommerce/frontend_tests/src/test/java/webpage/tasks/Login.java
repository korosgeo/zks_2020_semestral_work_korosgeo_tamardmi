package webpage.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.*;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.waits.WaitUntil;
import net.thucydides.core.annotations.Step;

import static webpage.pageObjects.LoginPage.*;

public class Login implements Task {

    private final String email;
    private final String password;

    public Login(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public static Login called(String email, String password) {
        return Instrumented.instanceOf(Login.class).withProperties(email, password);
    }

    @Override
    @Step("{0} Login to account")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(LOGIN_PAGE_LINK),
                Enter.theValue(email).into(EMAIL_FIELD),
                Enter.theValue(password).into(PASSWORD_FIELD),
                WaitUntil.the(LOGIN_SUBMIT_BUTTON, WebElementStateMatchers.isEnabled()),
                Click.on(LOGIN_SUBMIT_BUTTON)
        );
    }
}
