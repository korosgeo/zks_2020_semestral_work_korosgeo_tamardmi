package webpage.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.*;
import net.thucydides.core.annotations.Step;

import static webpage.pageObjects.CartPage.REMOVE_BUTTON;

public class RemoveItemFromTheCart implements Task {

    public RemoveItemFromTheCart() {
    }

    public static RemoveItemFromTheCart called() {
        return Instrumented.instanceOf(RemoveItemFromTheCart.class).newInstance();
    }

    @Override
    @Step("{0} clear cart")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(REMOVE_BUTTON)
        );
    }
}