package webpage.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.*;
import net.thucydides.core.annotations.Step;

import static webpage.pageObjects.CheckoutPage.ORDERS_BUTTON;

public class ViewOrders implements Task {

    public ViewOrders() {
    }

    public static ViewOrders called() {
        return Instrumented.instanceOf(ViewOrders.class).newInstance();
    }

    @Override
    @Step("{0} view orders")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(ORDERS_BUTTON)
        );
    }
}