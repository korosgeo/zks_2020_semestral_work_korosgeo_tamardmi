package webpage.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Step;
import webpage.pageObjects.HomePage;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class StartWith implements Task {

    HomePage homePage;

    public static StartWith mainPage() {
        return instrumented(StartWith.class);
    }

    @Override
    @Step("{0} starts with an main page")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
            Open.browserOn().the(homePage)
        );
    }
}