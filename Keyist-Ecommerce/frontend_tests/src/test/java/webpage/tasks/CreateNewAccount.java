package webpage.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.*;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.waits.WaitUntil;
import net.thucydides.core.annotations.Step;

import static webpage.pageObjects.SignupPage.*;

public class CreateNewAccount implements Task {

    private final String email;
    private final String password;

    public CreateNewAccount(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public static CreateNewAccount called(String email, String password) {
        return Instrumented.instanceOf(CreateNewAccount.class).withProperties(email, password);
    }

    @Override
    @Step("{0} create new account")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(SIGNUP_PAGE_BUTTON),
                Enter.theValue(email).into(EMAIL_FIELD),
                Enter.theValue(password).into(NEW_PASSWORD_FIELD),
                Enter.theValue(password).into(NEW_PASSWORD_CONFIRM_FIELD),
                WaitUntil.angularRequestsHaveFinished(),
                WaitUntil.the(REGISTER_SUBMIT_BUTTON, WebElementStateMatchers.isEnabled()),
                Click.on(REGISTER_SUBMIT_BUTTON)
        );
    }
}

