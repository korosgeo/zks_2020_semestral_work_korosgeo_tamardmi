package webpage.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.*;
import net.thucydides.core.annotations.Step;

import static webpage.pageObjects.CartPage.CART_CHECKOUT_BUTTON;

public class ClickCheckoutButton implements Task {

    public ClickCheckoutButton() {
    }

    public static ClickCheckoutButton called() {
        return Instrumented.instanceOf(ClickCheckoutButton.class).newInstance();
    }

    @Override
    @Step("{0} click checkout button")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(CART_CHECKOUT_BUTTON)
        );
    }
}