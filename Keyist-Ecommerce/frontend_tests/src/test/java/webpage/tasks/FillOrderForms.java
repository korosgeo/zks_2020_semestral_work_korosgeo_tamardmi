package webpage.tasks;

import net.bytebuddy.description.modifier.Ownership;
import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.*;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.waits.WaitUntil;
import net.thucydides.core.annotations.Step;

import java.util.HashMap;
import java.util.Map;

import static webpage.pageObjects.CheckoutPage.*;

public class FillOrderForms implements Task {

    Map<String, String> formsInfo;

    public FillOrderForms(Map<String, String> formsInfo) {
        this.formsInfo = formsInfo;
    }

    public static FillOrderForms called(Map<String, String> formsInfo) {
        return Instrumented.instanceOf(FillOrderForms.class).withProperties(formsInfo);
    }

    @Override
    @Step("{0} fill order forms")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(formsInfo.get("nameSurname")).into(NAME_SURNAME_FIELD),
                Enter.theValue(formsInfo.get("phoneNumber")).into(PHONE_NUMBER_FIELD),
                Click.on(PI_CONTINUE_BUTTON),
                Enter.theValue(formsInfo.get("country")).into(COUNTRY_FIELD),
                Enter.theValue(formsInfo.get("city")).into(CITY_FIELD),
                Enter.theValue(formsInfo.get("state")).into(STATE_FIELD),
                Enter.theValue(formsInfo.get("zip")).into(ZIP_FIELD),
                Enter.theValue(formsInfo.get("address")).into(ADDRESS_FIELD),
                Click.on(SHIPPING_CONTINUE_BUTTON),
                Enter.theValue(formsInfo.get("cardNumber")).into(CARD_NUMBER_FIELD),
                Enter.theValue(formsInfo.get("cardOwner")).into(CARD_OWNER_FIELD),
                Enter.theValue(formsInfo.get("ccv")).into(CCV_FIELD),
                Click.on(Year_DROP_DOWN),
                MoveMouse.to(YEAR_DROP_DOWN_OPTION),
                Click.on(YEAR_DROP_DOWN_OPTION),
                Click.on(MONTH_DROP_DOWN),
                MoveMouse.to(MONTH_DROP_DOWN_OPTION),
                Click.on(MONTH_DROP_DOWN_OPTION),
                Click.on(PAYMENT_CONTINUE_BUTTON)
        );
    }
}

