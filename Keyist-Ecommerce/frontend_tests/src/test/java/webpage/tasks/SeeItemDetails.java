package webpage.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.*;
import net.thucydides.core.annotations.Step;

import static webpage.pageObjects.HomePage.MOST_SELLING_PRODUCT;

public class SeeItemDetails implements Task {

    public SeeItemDetails() {
    }

    public static SeeItemDetails called() {
        return Instrumented.instanceOf(SeeItemDetails.class).newInstance();
    }

    @Override
    @Step("{0} see most selling item details")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Scroll.to(MOST_SELLING_PRODUCT),
                Click.on(MOST_SELLING_PRODUCT)
        );
    }
}
