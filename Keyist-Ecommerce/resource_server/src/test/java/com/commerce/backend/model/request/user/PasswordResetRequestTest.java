package com.commerce.backend.model.request.user;


import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PasswordResetRequestTest {

    @ParameterizedTest
    @CsvFileSource(resources = "/Keyist-password_reset-output.csv", numLinesToSkip = 1)
    public void parametrized_test_for_password_reset_request(String oldPassword, String newPassword, String newPasswordConfirm, String expected) {
        PasswordResetRequest passwordResetRequest = new PasswordResetRequest();

        if (oldPassword == null) {
            oldPassword = "";
        }
        if (newPassword == null) {
            newPassword = "";
        }
        if (newPasswordConfirm == null) {
            newPasswordConfirm = "";
        }

        passwordResetRequest.setOldPassword(oldPassword);
        passwordResetRequest.setNewPassword(newPassword);
        passwordResetRequest.setNewPasswordConfirm(newPasswordConfirm);

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<PasswordResetRequest>> violations = validator.validate(passwordResetRequest);

        String actual;

        if (violations.size() > 0) {
            actual = "exception";
        } else {
            actual = "success";
        }

        assertEquals(actual, expected);
    }
}
