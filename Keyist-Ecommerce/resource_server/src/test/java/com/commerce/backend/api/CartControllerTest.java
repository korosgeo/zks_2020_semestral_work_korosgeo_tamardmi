package com.commerce.backend.api;

import com.commerce.backend.model.dto.DiscountDTO;
import com.commerce.backend.model.request.cart.*;
import com.commerce.backend.model.response.cart.CartResponse;
import com.commerce.backend.service.CartService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.BDDMockito.when;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
@WebMvcTest(CartController.class)
@AutoConfigureWebClient
@WithMockUser
@ComponentScan(basePackages = {"com.commerce.backend.constants"})
class CartControllerTest {

    @Autowired
    ObjectMapper objectMapper;
    @MockBean
    private CartService cartService;
    @Autowired
    private MockMvc mockMvc;
    private Faker faker;

    @BeforeEach
    public void setUp() {
        faker = new Faker();
    }

    @Test
    void it_should_add_to_cart() throws Exception {
        AddToCartRequest addToCartRequest = new AddToCartRequest();
        addToCartRequest.setAmount(faker.number().randomDigitNotZero());
        addToCartRequest.setProductVariantId(faker.number().randomNumber());

        CartResponse cartResponseExpected = new CartResponse();

        when(cartService.addToCart(addToCartRequest.getProductVariantId(), addToCartRequest.getAmount())).thenReturn(cartResponseExpected);
        
        MvcResult result = mockMvc.perform(post("/api/cart")
                .content(objectMapper.writeValueAsString(addToCartRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andReturn();
        
        verify(cartService, times(1)).addToCart(addToCartRequest.getProductVariantId(), addToCartRequest.getAmount());
        then(result.getResponse().getContentAsString()).isEqualTo(objectMapper.writeValueAsString(cartResponseExpected));
    }

    @Test
    void it_should_not_add_to_cart_when_amount_is_invalid() throws Exception {
        AddToCartRequest addToCartRequest = new AddToCartRequest();
        addToCartRequest.setAmount(null);
        addToCartRequest.setProductVariantId(faker.number().randomNumber());

        MvcResult result = mockMvc.perform(post("/api/cart")
                .content(objectMapper.writeValueAsString(addToCartRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andReturn();

        then(result.getResponse().getContentAsString()).contains("must not be null");

    }

    @Test
    void it_should_not_add_to_cart_when_product_variant_id_is_invalid() throws Exception {
        AddToCartRequest addToCartRequest = new AddToCartRequest();
        addToCartRequest.setAmount(faker.number().randomDigitNotZero());
        addToCartRequest.setProductVariantId(null);

        MvcResult result = mockMvc.perform(post("/api/cart")
                .content(objectMapper.writeValueAsString(addToCartRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andReturn();

        then(result.getResponse().getContentAsString()).contains("must not be null");

    }

    @Test
    void it_should_increase_cart_item() throws Exception {
        IncrementCartItemRequest incrementCartItemRequest = new IncrementCartItemRequest();
        incrementCartItemRequest.setAmount(faker.number().randomDigitNotZero());
        incrementCartItemRequest.setCartItemId(faker.number().randomNumber());

        CartResponse cartResponseExpected = new CartResponse();

        when(cartService.incrementCartItem(incrementCartItemRequest.getCartItemId(), incrementCartItemRequest.getAmount())).thenReturn(cartResponseExpected);

        MvcResult result = mockMvc.perform(post("/api/cart/increment")
                .content(objectMapper.writeValueAsString(incrementCartItemRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andReturn();
        
        verify(cartService, times(1)).incrementCartItem(incrementCartItemRequest.getCartItemId(), incrementCartItemRequest.getAmount());
        then(result.getResponse().getContentAsString()).isEqualTo(objectMapper.writeValueAsString(cartResponseExpected));
    }

    @Test
    void it_should_not_increment_cart_item_when_amount_is_invalid() throws Exception {
        IncrementCartItemRequest incrementCartItemRequest = new IncrementCartItemRequest();
        incrementCartItemRequest.setAmount(null);
        incrementCartItemRequest.setCartItemId(faker.number().randomNumber());

        MvcResult result = mockMvc.perform(post("/api/cart/increment")
                .content(objectMapper.writeValueAsString(incrementCartItemRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andReturn();

        then(result.getResponse().getContentAsString()).contains("must not be null");

    }

    @Test
    void it_should_not_increment_cart_item_when_cart_item_id_is_invalid() throws Exception {
        IncrementCartItemRequest incrementCartItemRequest = new IncrementCartItemRequest();
        incrementCartItemRequest.setAmount(faker.number().randomDigitNotZero());
        incrementCartItemRequest.setCartItemId(null);

        MvcResult result = mockMvc.perform(post("/api/cart/increment")
                .content(objectMapper.writeValueAsString(incrementCartItemRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andReturn();
        
        then(result.getResponse().getContentAsString()).contains("must not be null");

    }
}
