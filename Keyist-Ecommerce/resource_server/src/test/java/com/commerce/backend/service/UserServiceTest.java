package com.commerce.backend.service;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import com.commerce.backend.converter.user.UserResponseConverter;
import com.commerce.backend.dao.UserRepository;
import com.commerce.backend.error.exception.InvalidArgumentException;
import com.commerce.backend.error.exception.ResourceNotFoundException;
import com.commerce.backend.model.entity.User;
import com.commerce.backend.model.request.user.PasswordResetRequest;
import com.commerce.backend.model.request.user.RegisterUserRequest;
import com.commerce.backend.model.request.user.UpdateUserAddressRequest;
import com.commerce.backend.model.request.user.UpdateUserRequest;
import com.commerce.backend.model.response.user.UserResponse;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collection;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.when;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class UserServiceTest {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private UserResponseConverter userResponseConverter;

    private Faker faker;

    private String userName;


    @BeforeEach
    public void setUp() {
        faker = new Faker();
        userName = faker.name().username();
        SecurityContextHolder.setContext(new SecurityContextImpl());
        SecurityContextHolder.getContext().setAuthentication(new Authentication() {
            @Override
            public Collection<? extends GrantedAuthority> getAuthorities() {
                return null;
            }

            @Override
            public Object getCredentials() {
                return null;
            }

            @Override
            public Object getDetails() {
                return null;
            }

            @Override
            public Object getPrincipal() {
                return null;
            }

            @Override
            public boolean isAuthenticated() {
                return false;
            }

            @Override
            public String getName() {
                return userName;
            }

            @Override
            public void setAuthenticated(boolean b) throws IllegalArgumentException {

            }


        });
    }


    @Test
    void it_should_register_a_user() {
        String email = faker.lorem().word();
        String password = faker.lorem().word();
        RegisterUserRequest registerUserRequest = new RegisterUserRequest();
        registerUserRequest.setEmail(email);
        registerUserRequest.setPassword(password);

        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        User userExpected = new User();

        when(userRepository.existsByEmail(email)).thenReturn(false);
        when(passwordEncoder.encode(password)).thenReturn(password);
        when(userRepository.save(any(User.class))).thenReturn(userExpected);

        User user = userService.register(registerUserRequest);

        verify(userRepository).save(userArgumentCaptor.capture());
        then(user).isEqualToComparingFieldByField(userExpected);
        then(userArgumentCaptor.getValue().getEmail()).isEqualTo(email);
        then(userArgumentCaptor.getValue().getPassword()).isEqualTo(password);
        then(userArgumentCaptor.getValue().getEmailVerified()).isEqualTo(0);

    }


    @Test
    void it_should_not_register_a_user_when_it_exists() {
        String email = faker.lorem().word();
        RegisterUserRequest registerUserRequest = new RegisterUserRequest();
        registerUserRequest.setEmail(email);

        when(userRepository.existsByEmail(email)).thenReturn(true);

        assertThatThrownBy(() -> userService.register(registerUserRequest))
                .isInstanceOf(InvalidArgumentException.class)
                .hasMessage("An account already exists with this email");

    }

    @Test
    void it_should_fetch_user() {
        User user = new User();

        UserResponse userResponseExpected = new UserResponse();

        when(userRepository.findByEmail(userName)).thenReturn(Optional.of(user));
        when(userResponseConverter.apply(user)).thenReturn(userResponseExpected);

        UserResponse userResponseResult = userService.fetchUser();

        verify(userRepository).findByEmail(userName);
        then(userResponseResult).isEqualTo(userResponseExpected);

    }

    @Test
    void it_should_throw_exception_when_user_name_is_null_fetch_user() {
        SecurityContextHolder.getContext().setAuthentication(new Authentication() {
            @Override
            public Collection<? extends GrantedAuthority> getAuthorities() {
                return null;
            }

            @Override
            public Object getCredentials() {
                return null;
            }

            @Override
            public Object getDetails() {
                return null;
            }

            @Override
            public Object getPrincipal() {
                return null;
            }

            @Override
            public boolean isAuthenticated() {
                return false;
            }

            @Override
            public String getName() {
                return null;
            }

            @Override
            public void setAuthenticated(boolean b) throws IllegalArgumentException {

            }


        });

        assertThatThrownBy(() -> userService.fetchUser())
                .isInstanceOf(AccessDeniedException.class)
                .hasMessage("Invalid access");

    }

    @Test
    void it_should_throw_exception_when_user_is_null_found_by_email_fetch_user() {
        when(userRepository.findByEmail(userName)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> userService.fetchUser())
                .isInstanceOf(ResourceNotFoundException.class)
                .hasMessage("User not found");

    }

    @Test
    void it_should_get_user() {
        User user = new User();

        when(userRepository.findByEmail(userName)).thenReturn(Optional.of(user));

        User userResult = userService.getUser();

        verify(userRepository).findByEmail(userName);
        then(userResult).isEqualTo(user);

    }

}