package com.commerce.backend.service;

import com.commerce.backend.converter.product.ProductDetailsResponseConverter;
import com.commerce.backend.converter.product.ProductResponseConverter;
import com.commerce.backend.converter.product.ProductVariantResponseConverter;
import com.commerce.backend.dao.ProductRepository;
import com.commerce.backend.dao.ProductVariantRepository;
import com.commerce.backend.error.exception.InvalidArgumentException;
import com.commerce.backend.error.exception.ResourceNotFoundException;
import com.commerce.backend.model.entity.Product;
import com.commerce.backend.model.entity.ProductVariant;
import com.commerce.backend.model.response.product.ProductDetailsResponse;
import com.commerce.backend.model.response.product.ProductVariantResponse;
import com.commerce.backend.service.cache.ProductCacheService;
import com.commerce.backend.service.cache.ProductVariantCacheService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {

    @InjectMocks
    private ProductServiceImpl productService;

    @Mock
    private ProductCacheService productCacheService;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private ProductVariantRepository productVariantRepository;

    @Mock
    private ProductVariantCacheService productVariantCacheService;

    @Mock
    private ProductResponseConverter productResponseConverter;

    @Mock
    private ProductVariantResponseConverter productVariantResponseConverter;

    @Mock
    private ProductDetailsResponseConverter productDetailsResponseConverter;

    @Test
    public void it_should_find_product_by_url() {
        String url = "example.url.com";

        Product product = new Product();

        ProductDetailsResponse productDetailsResponseExpected = new ProductDetailsResponse();

        when(productCacheService.findByUrl(url)).thenReturn(product);
        when(productDetailsResponseConverter.apply(product)).thenReturn(productDetailsResponseExpected);

        ProductDetailsResponse productDetailsResponseResult = productService.findByUrl(url);

        verify(productDetailsResponseConverter).apply(product);
        then(productDetailsResponseResult).isEqualTo(productDetailsResponseExpected);

    }

    @Test
    public void it_should_throw_exception_when_no_product_found_by_url() {
        String url = "example.url.com";

        when(productCacheService.findByUrl(url)).thenReturn(null);

        assertThatThrownBy(() -> productService.findByUrl(url))
                .isInstanceOf(ResourceNotFoundException.class)
                .hasMessage(String.format("Product not found with the url %s", url));
    }

    @Test
    public void it_should_find_product__variant_by_id() {
        Long id = 1L;

        ProductVariant productVariant = new ProductVariant();

        when(productVariantCacheService.findById(id)).thenReturn(productVariant);

        ProductVariant productVariantResult = productService.findProductVariantById(id);

        then(productVariantResult).isEqualTo(productVariant);

    }

    @Test
    public void it_should_throw_exception_when_no_product__variant_found_by_id() {
        Long id = 1L;

        when(productVariantCacheService.findById(id)).thenReturn(null);

        assertThatThrownBy(() -> productService.findProductVariantById(id))
                .isInstanceOf(ResourceNotFoundException.class)
                .hasMessage(String.format("Could not find any product variant with the id %d", id));
    }

    @Test
    public void it_should_get_all_product_variants() {
        Integer page = 0;
        Integer size = 10;
        String sort = "lowest";
        String category = "Animals";
        Float minPrice = 4.f;
        Float maxPrice = 7.f;
        String color = "red";

        ProductVariant productVariant = new ProductVariant();
        List<ProductVariant> productVariantList = new ArrayList<>();
        productVariantList.add(productVariant);

        Page<ProductVariant> productVariantPage = new PageImpl<>(productVariantList);

        ProductVariantResponse productVariantResponseExpected = new ProductVariantResponse();

        when(productVariantRepository.findAll(any(Specification.class), any(PageRequest.class))).thenReturn(productVariantPage);
        when(productVariantResponseConverter.apply(any(ProductVariant.class))).thenReturn(productVariantResponseExpected);

        List<ProductVariantResponse> productVariantResponseList = productService.getAll(page, size, sort, category, minPrice, maxPrice, color);

        then(productVariantResponseList.size()).isEqualTo(productVariantList.size());
        productVariantResponseList.forEach(productVariantResponse -> then(productVariantResponse).isEqualTo(productVariantResponseExpected));
    }

    @Test
    public void it_should_get_all_product_variants_with_no_sort() {
        Integer page = 0;
        Integer size = 10;
        String sort = null;
        String category = "Animals";
        Float minPrice = 4.f;
        Float maxPrice = 7.f;
        String color = "red";

        ProductVariant productVariant = new ProductVariant();
        List<ProductVariant> productVariantList = new ArrayList<>();
        productVariantList.add(productVariant);

        Page<ProductVariant> productVariantPage = new PageImpl<>(productVariantList);

        ProductVariantResponse productVariantResponseExpected = new ProductVariantResponse();

        when(productVariantRepository.findAll(any(Specification.class), any(PageRequest.class))).thenReturn(productVariantPage);
        when(productVariantResponseConverter.apply(any(ProductVariant.class))).thenReturn(productVariantResponseExpected);

        List<ProductVariantResponse> productVariantResponseList = productService.getAll(page, size, sort, category, minPrice, maxPrice, color);

        then(productVariantResponseList.size()).isEqualTo(productVariantList.size());
        productVariantResponseList.forEach(productVariantResponse -> then(productVariantResponse).isEqualTo(productVariantResponseExpected));
    }

    @Test
    public void it_should_throw_exception_when_invalid_sort() {
        Integer page = 0;
        Integer size = 10;
        String sort = "invalid";
        String category = "Animals";
        Float minPrice = 4.f;
        Float maxPrice = 7.f;
        String color = "red";

        assertThatThrownBy(() -> productService.getAll(page, size, sort, category, minPrice, maxPrice, color))
                .isInstanceOf(InvalidArgumentException.class)
                .hasMessage("Invalid sort parameter");
    }
}
