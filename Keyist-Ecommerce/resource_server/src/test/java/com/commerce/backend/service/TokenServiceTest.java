package com.commerce.backend.service;

import com.commerce.backend.dao.PasswordForgotTokenRepository;
import com.commerce.backend.dao.VerificationTokenRepository;
import com.commerce.backend.error.exception.InvalidArgumentException;
import com.commerce.backend.error.exception.ResourceNotFoundException;
import com.commerce.backend.model.entity.PasswordForgotToken;
import com.commerce.backend.model.entity.User;
import com.commerce.backend.model.entity.VerificationToken;
import com.commerce.backend.model.event.OnPasswordForgotRequestEvent;
import com.commerce.backend.model.event.OnRegistrationCompleteEvent;
import com.commerce.backend.model.request.user.PasswordForgotValidateRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TokenServiceTest {

    @InjectMocks
    private TokenServiceImpl tokenService;

    @Mock
    private UserService userService;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private ApplicationEventPublisher eventPublisher;

    @Mock
    private VerificationTokenRepository verificationTokenRepository;

    @Mock
    private PasswordForgotTokenRepository passwordForgotTokenRepository;

    private User user;

    @Test
    public void it_should_create_email_confirm_token() {
        user = new User();

        ArgumentCaptor<VerificationToken> verificationTokenArgumentCaptor = ArgumentCaptor.forClass(VerificationToken.class);
        ArgumentCaptor<OnRegistrationCompleteEvent> onRegistrationCompleteEventArgumentCaptor = ArgumentCaptor.forClass(OnRegistrationCompleteEvent.class);

        when(verificationTokenRepository.save(any(VerificationToken.class))).thenReturn(new VerificationToken());

        tokenService.createEmailConfirmToken(user);

        verify(verificationTokenRepository).save(verificationTokenArgumentCaptor.capture());
        verify(eventPublisher).publishEvent(onRegistrationCompleteEventArgumentCaptor.capture());

        then(user).isEqualTo(onRegistrationCompleteEventArgumentCaptor.getValue().getUser());
        then(verificationTokenArgumentCaptor.getValue().getToken()).isEqualTo(onRegistrationCompleteEventArgumentCaptor.getValue().getToken());

    }

    @Test
    public void it_should_create_password_reset_token_if_it_does_not_exist() {
        user = new User();
        String email = "email@email.com";
        ArgumentCaptor<PasswordForgotToken> passwordForgotTokenArgumentCaptor = ArgumentCaptor.forClass(PasswordForgotToken.class);
        ArgumentCaptor<OnPasswordForgotRequestEvent> onPasswordForgotRequestEventArgumentCaptor = ArgumentCaptor.forClass(OnPasswordForgotRequestEvent.class);

        when(userService.findByEmail(email)).thenReturn(user);
        when(passwordForgotTokenRepository.findByUser(user)).thenReturn(Optional.empty());
        when(passwordForgotTokenRepository.save(any(PasswordForgotToken.class))).thenReturn(new PasswordForgotToken());

        tokenService.createPasswordResetToken(email);

        verify(passwordForgotTokenRepository).save(passwordForgotTokenArgumentCaptor.capture());
        verify(eventPublisher).publishEvent(onPasswordForgotRequestEventArgumentCaptor.capture());

        then(user).isEqualTo(onPasswordForgotRequestEventArgumentCaptor.getValue().getUser());
        then(onPasswordForgotRequestEventArgumentCaptor.getValue().getToken()).isEqualTo(onPasswordForgotRequestEventArgumentCaptor.getValue().getToken());

    }

    @Test
    public void it_should_create_password_reset_token_if_it_already_exists() {
        user = new User();
        String email = "email@email.com";
        ArgumentCaptor<PasswordForgotToken> passwordForgotTokenArgumentCaptor = ArgumentCaptor.forClass(PasswordForgotToken.class);
        ArgumentCaptor<OnPasswordForgotRequestEvent> onPasswordForgotRequestEventArgumentCaptor = ArgumentCaptor.forClass(OnPasswordForgotRequestEvent.class);

        when(userService.findByEmail(email)).thenReturn(user);
        when(passwordForgotTokenRepository.findByUser(user)).thenReturn(Optional.of(new PasswordForgotToken()));
        when(passwordForgotTokenRepository.save(any(PasswordForgotToken.class))).thenReturn(new PasswordForgotToken());

        tokenService.createPasswordResetToken(email);

        verify(passwordForgotTokenRepository).save(passwordForgotTokenArgumentCaptor.capture());
        verify(eventPublisher).publishEvent(onPasswordForgotRequestEventArgumentCaptor.capture());


        then(user).isEqualTo(onPasswordForgotRequestEventArgumentCaptor.getValue().getUser());
        then(onPasswordForgotRequestEventArgumentCaptor.getValue().getToken()).isEqualTo(onPasswordForgotRequestEventArgumentCaptor.getValue().getToken());

    }

    @Test
    public void it_should_validate_email_by_token() {
        user = new User();
        String token = "random_token";
        VerificationToken verificationToken = new VerificationToken();
        verificationToken.setUser(user);
        verificationToken.setExpiryDate(Date.from(Instant.now().plus(Duration.ofHours(4))));

        ArgumentCaptor<VerificationToken> verificationTokenArgumentCaptor = ArgumentCaptor.forClass(VerificationToken.class);
        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);

        when(verificationTokenRepository.findByToken(token)).thenReturn(Optional.of(verificationToken));
        when(userService.saveUser(user)).thenReturn(user);

        tokenService.validateEmail(token);

        verify(verificationTokenRepository).delete(verificationTokenArgumentCaptor.capture());
        verify(userService).saveUser(userArgumentCaptor.capture());

        then(verificationTokenArgumentCaptor.getValue().getToken()).isEqualTo(verificationToken.getToken());
        then(verificationTokenArgumentCaptor.getValue().getUser()).isEqualTo(verificationToken.getUser());
        then(verificationTokenArgumentCaptor.getValue().getUser().getEmailVerified()).isEqualTo(1);

    }

    @Test
    public void it_should_throw_exception_when_no_token_on_validate_email() {
        user = new User();
        String token = "random_token";

        when(verificationTokenRepository.findByToken(token)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> tokenService.validateEmail(token))
                .isInstanceOf(ResourceNotFoundException.class)
                .hasMessage("Null verification token");
    }

    @Test
    public void it_should_throw_exception_when_no_user_on_validate_email() {
        user = new User();
        String token = "random_token";
        VerificationToken verificationToken = new VerificationToken();
        verificationToken.setUser(null);
        verificationToken.setExpiryDate(Date.from(Instant.now().plus(Duration.ofHours(4))));

        when(verificationTokenRepository.findByToken(token)).thenReturn(Optional.of(verificationToken));

        assertThatThrownBy(() -> tokenService.validateEmail(token))
                .isInstanceOf(ResourceNotFoundException.class)
                .hasMessage("User not found");
    }
}
