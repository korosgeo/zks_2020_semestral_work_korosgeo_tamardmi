package com.commerce.backend.service;

import com.commerce.backend.converter.order.OrderResponseConverter;
import com.commerce.backend.dao.OrderRepository;
import com.commerce.backend.error.exception.InvalidArgumentException;
import com.commerce.backend.error.exception.ResourceFetchException;
import com.commerce.backend.model.entity.*;
import com.commerce.backend.model.request.order.PostOrderRequest;
import com.commerce.backend.model.response.order.OrderResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceTest {

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private UserService userService;

    @Mock
    private CartService cartService;

    @Mock
    private OrderResponseConverter orderResponseConverter;

    @InjectMocks
    private OrderServiceImpl orderService;

    @Test
    public void when_fetching_all_orders_for_a_user_it_should_return_order_count() {
        User user = new User();

        List<Order> orderList = new ArrayList<>();
        orderList.add(new Order());
        orderList.add(new Order());

        when(userService.getUser()).thenReturn(user);
        when(orderRepository.countAllByUser(user)).thenReturn(Optional.of(orderList.size()));

        Integer ordersCount = orderService.getAllOrdersCount();

        then(ordersCount).isEqualTo(orderList.size());
    }

    @Test
    public void when_fetching_all_orders_it_should_return_error() {
        User user = new User();

        when(userService.getUser()).thenReturn(user);
        when(orderRepository.countAllByUser(user)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> orderService.getAllOrdersCount())
                .isInstanceOf(ResourceFetchException.class)
                .hasMessage("An error occurred whilst fetching orders count");
    }

    @Test
    public void when_getting_all_orders_it_should_return_all_orders_for_a_user() {
        User user = new User();
        int page = 0;
        int size = 10;

        List<Order> orderList = new ArrayList<>();
        orderList.add(new Order());
        orderList.add(new Order());

        OrderResponse orderResponse = new OrderResponse();

        when(userService.getUser()).thenReturn(user);
        when(orderRepository.findAllByUserOrderByDateDesc(user, PageRequest.of(page, size))).thenReturn(orderList);
        when(orderResponseConverter.apply(any(Order.class))).thenReturn(orderResponse);

        List<OrderResponse> orderResponseList = orderService.getAllOrders(page, size);

        then(orderResponseList.size()).isEqualTo(orderList.size());
        orderResponseList.forEach(orderResponse1 -> then(orderResponse1).isEqualTo(orderResponse));
    }

    @Test
    public void when_post_order_it_should_return_order() {
        ProductVariant productVariant = new ProductVariant();
        productVariant.setSellCount(2);
        productVariant.setStock(5);
        Cart cart = new Cart();
        CartItem cartItem = new CartItem();
        cartItem.setProductVariant(productVariant);
        cartItem.setAmount(productVariant.getStock() - 1);
        List<CartItem> cartItemList = new ArrayList<>();
        cartItemList.add(cartItem);
        cart.setCartItemList(cartItemList);
        cart.setTotalPrice(22.f);
        cart.setTotalCargoPrice(5.f);

        User user = new User();
        user.setCart(cart);

        PostOrderRequest postOrderRequest = new PostOrderRequest();
        postOrderRequest.setShipName("First Name");


        OrderResponse orderResponseExpected = new OrderResponse();

        Order order = new Order();

        ArgumentCaptor<Order> orderArgumentCaptor = ArgumentCaptor.forClass(Order.class);

        when(userService.getUser()).thenReturn(user);
        when(orderRepository.save(orderArgumentCaptor.capture())).thenReturn(order);
        when(orderResponseConverter.apply(order)).thenReturn(orderResponseExpected);

        // when
        OrderResponse orderResponseResult = orderService.postOrder(postOrderRequest);

        // then
        verify(orderRepository).save(orderArgumentCaptor.getValue());
        verify(orderResponseConverter).apply(order);

        then(orderResponseResult).isEqualTo(orderResponseExpected);
    }

    @Test
    public void it_should_throw_exception_when_post_order_has_null_cart() {
        User user = new User();

        // given
        when(userService.getUser()).thenReturn(user);

        // when, then
        assertThatThrownBy(() -> orderService.postOrder(new PostOrderRequest()))
                .isInstanceOf(InvalidArgumentException.class)
                .hasMessage("Cart is not valid");

    }


}
