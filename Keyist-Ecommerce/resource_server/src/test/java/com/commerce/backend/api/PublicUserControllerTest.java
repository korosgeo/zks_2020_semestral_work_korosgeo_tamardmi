package com.commerce.backend.api;

import com.commerce.backend.model.entity.User;
import com.commerce.backend.model.request.user.*;
import com.commerce.backend.service.TokenService;
import com.commerce.backend.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.BDDMockito.when;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
@WebMvcTest(PublicUserController.class)
@AutoConfigureWebClient
@ComponentScan(basePackages = {"com.commerce.backend.constants"})
class PublicUserControllerTest {

    @Autowired
    ObjectMapper objectMapper;
    @MockBean
    private UserService userService;
    @MockBean
    private TokenService tokenService;
    @Autowired
    private MockMvc mockMvc;
    private Faker faker;

    @BeforeEach
    public void setUp() {
        faker = new Faker();
    }

    @Test
    void it_should_register_user() throws Exception {
        String email = String.format("%s@%s.com", faker.lorem().characters(1, 10), faker.lorem().characters(1, 10));
        String password = faker.lorem().characters(6, 52);
        String passwordRepeat = password + "";

        RegisterUserRequest registerUserRequest = new RegisterUserRequest();
        registerUserRequest.setEmail(email);
        registerUserRequest.setPassword(password);
        registerUserRequest.setPasswordRepeat(passwordRepeat);

        User user = new User();


        when(userService.register(registerUserRequest)).thenReturn(user);

        mockMvc.perform(post("/api/public/account/registration")
                .content(objectMapper.writeValueAsString(registerUserRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andReturn();

        verify(userService, times(1)).register(registerUserRequest);
        verify(tokenService, times(1)).createEmailConfirmToken(user);
    }

    @Test
    void it_should_not_register_user_if_invalid_request() throws Exception {
        String email = String.valueOf(faker.number().randomDigitNotZero());
        String password = faker.lorem().characters(6, 52);
        String passwordRepeat = password + "";

        RegisterUserRequest registerUserRequest = new RegisterUserRequest();
        registerUserRequest.setEmail(email);
        registerUserRequest.setPassword(password);
        registerUserRequest.setPasswordRepeat(passwordRepeat);

        User user = new User();


        when(userService.register(registerUserRequest)).thenReturn(user);

        MvcResult result = mockMvc.perform(post("/api/public/account/registration")
                .content(objectMapper.writeValueAsString(registerUserRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andReturn();
        
        verify(userService, times(0)).register(registerUserRequest);
        verify(tokenService, times(0)).createEmailConfirmToken(user);
        then(result.getResponse().getContentAsString()).contains("Invalid email");
    }

    @Test
    void it_should_validate_email() throws Exception {
        String token = faker.lorem().word();
        ValidateEmailRequest validateEmailRequest = new ValidateEmailRequest();
        validateEmailRequest.setToken(token);

        mockMvc.perform(post("/api/public/account/registration/validate")
                .content(objectMapper.writeValueAsString(validateEmailRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andReturn();

        verify(tokenService, times(1)).validateEmail(token);
    }

    @Test
    void it_should_not_validate_email_if_invalid_token() throws Exception {
        String token = "";
        ValidateEmailRequest validateEmailRequest = new ValidateEmailRequest();
        validateEmailRequest.setToken(token);

        MvcResult result = mockMvc.perform(post("/api/public/account/registration/validate")
                .content(objectMapper.writeValueAsString(validateEmailRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andReturn();
        
        verify(tokenService, times(0)).validateEmail(token);
        then(result.getResponse().getContentAsString()).contains("must not be blank");
    }

    @Test
    void it_should_create_password_reset_token() throws Exception {
        String email = String.format("%s@%s.com", faker.lorem().characters(1, 10), faker.lorem().characters(1, 10));
        PasswordForgotRequest passwordForgotRequest = new PasswordForgotRequest();
        passwordForgotRequest.setEmail(email);

        mockMvc.perform(post("/api/public/account/password/forgot")
                .content(objectMapper.writeValueAsString(passwordForgotRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful())
                .andReturn();

        verify(tokenService, times(1)).createPasswordResetToken(email);
    }

    @Test
    void it_should_not_create_password_reset_token_if_invalid_email() throws Exception {
        String email = String.valueOf(faker.number().randomDigitNotZero());
        PasswordForgotRequest passwordForgotRequest = new PasswordForgotRequest();
        passwordForgotRequest.setEmail(email);

        MvcResult result = mockMvc.perform(post("/api/public/account/password/forgot")
                .content(objectMapper.writeValueAsString(passwordForgotRequest))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andReturn();
        
        verify(tokenService, times(0)).createPasswordResetToken(email);
        then(result.getResponse().getContentAsString()).contains("Invalid email");
    }
}