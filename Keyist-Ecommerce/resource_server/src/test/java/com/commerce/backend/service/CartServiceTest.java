package com.commerce.backend.service;

import com.commerce.backend.converter.cart.CartResponseConverter;
import com.commerce.backend.dao.CartRepository;
import com.commerce.backend.error.exception.InvalidArgumentException;
import com.commerce.backend.model.entity.Cart;
import com.commerce.backend.model.entity.CartItem;
import com.commerce.backend.model.entity.ProductVariant;
import com.commerce.backend.model.entity.User;
import com.commerce.backend.model.response.cart.CartResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CartServiceTest {

    @Mock
    private CartRepository cartRepository;

    @Mock
    private ProductService productService;

    @Mock
    private UserService userService;

    @Mock
    private CartResponseConverter cartResponseConverter;

    @InjectMocks
    private CartServiceImpl cartService;

    private User user;

    private Cart cart;

//    @BeforeEach
//    public void setUp() {
//        user = new User();
//    }

    @Test
    public void when_adding_to_an_empty_cart_it_should_return_an_updated_cart() {
        Long productVariantId = 2L;
        Integer amount = 1;

        user = new User();
        cart = new Cart();
        user.setCart(cart);
        cart.setUser(user);

        ProductVariant productVariant = new ProductVariant();
        productVariant.setStock(1000);
        productVariant.setPrice(5.99f);
        productVariant.setCargoPrice(5.f);

        CartResponse cartResponseExpected = new CartResponse();

        when(userService.getUser()).thenReturn(user);
        when(productService.findProductVariantById(productVariantId)).thenReturn(productVariant);
        when(cartRepository.save(cart)).thenReturn(cart);
        when(cartResponseConverter.apply(cart)).thenReturn(cartResponseExpected);

        CartResponse cartResponse = cartService.addToCart(productVariantId, amount);
        then(cartResponse).isEqualTo(cartResponseExpected);
    }

    @Test
    public void when_adding_to_a_non_empty_cart_it_should_return_an_updated_cart() {
        Long productVariantId = 2L;
        Integer amount = 1;

        user = new User();
        cart = new Cart();

        ProductVariant productVariant = new ProductVariant();
        productVariant.setId(1L);
        productVariant.setStock(1000);
        productVariant.setPrice(9.99f);
        productVariant.setCargoPrice(5.f);

        List<CartItem> cartItemList = new ArrayList<>();
        CartItem cartItem = new CartItem();
        cartItem.setProductVariant(productVariant);
        cartItem.setAmount(2);
        cartItemList.add(cartItem);

        cart.setCartItemList(cartItemList);

        user.setCart(cart);
        cart.setUser(user);

        CartResponse cartResponseExpected = new CartResponse();

        when(userService.getUser()).thenReturn(user);
        when(productService.findProductVariantById(productVariantId)).thenReturn(productVariant);
        when(cartRepository.save(cart)).thenReturn(cart);
        when(cartResponseConverter.apply(cart)).thenReturn(cartResponseExpected);

        CartResponse cartResponse = cartService.addToCart(productVariantId, amount);
        then(cartResponse).isEqualTo(cartResponseExpected);
    }

    @Test
    public void when_adding_to_a_null_cart_it_should_return_a_new_cart() {
        user = new User();

        Long productVariantId = 2L;
        Integer amount = 1;

        ProductVariant productVariant = new ProductVariant();
        productVariant.setId(1L);
        productVariant.setStock(1000);
        productVariant.setPrice(9.99f);
        productVariant.setCargoPrice(5.f);

        CartResponse cartResponseExpected = new CartResponse();

        when(userService.getUser()).thenReturn(user);
        when(productService.findProductVariantById(productVariantId)).thenReturn(productVariant);
        when(cartRepository.save(cart)).thenReturn(cart);
        when(cartResponseConverter.apply(cart)).thenReturn(cartResponseExpected);

        CartResponse cartResponse = cartService.addToCart(productVariantId, amount);
        then(cartResponse).isEqualTo(cartResponseExpected);
    }

    @Test
    public void when_adding_to_a_cart_it_should_throw_not_in_stock_exception() {
        user = new User();

        Long productVariantId = 2L;
        Integer amount = 2;

        ProductVariant productVariant = new ProductVariant();
        productVariant.setId(1L);
        productVariant.setStock(1);
        productVariant.setPrice(9.99f);
        productVariant.setCargoPrice(5.f);

        when(userService.getUser()).thenReturn(user);
        when(productService.findProductVariantById(productVariantId)).thenReturn(productVariant);

        assertThatThrownBy(() -> cartService.addToCart(productVariantId, amount))
                .isInstanceOf(InvalidArgumentException.class)
                .hasMessage("Product does not have desired stock.");
    }

    @Test
    public void when_incrementing_cart_item_it_should_return_updated_cart() {
        user = new User();
        cart = new Cart();

        Long productVariantId = 2L;
        Long cartItemId = 1L;
        Integer amount = 2;

        ProductVariant productVariant = new ProductVariant();
        productVariant.setId(1L);
        productVariant.setStock(100);
        productVariant.setPrice(9.99f);
        productVariant.setCargoPrice(5.f);

        List<CartItem> cartItemList = new ArrayList<>();
        CartItem cartItem = new CartItem();
        cartItem.setProductVariant(productVariant);
        cartItem.setAmount(2);
        cartItem.setId(cartItemId);
        cartItemList.add(cartItem);

        cart.setCartItemList(cartItemList);

        user.setCart(cart);
        cart.setUser(user);

        CartResponse cartResponseExpected = new CartResponse();

        when(userService.getUser()).thenReturn(user);
        when(productService.findProductVariantById(productVariantId)).thenReturn(productVariant);
        when(cartRepository.save(cart)).thenReturn(cart);
        when(cartResponseConverter.apply(cart)).thenReturn(cartResponseExpected);

        CartResponse cartResponse = cartService.incrementCartItem(1L, amount);
        then(cartResponse).isEqualTo(cartResponseExpected);
    }

}
