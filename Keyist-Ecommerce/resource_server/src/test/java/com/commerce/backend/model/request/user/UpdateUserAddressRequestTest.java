package com.commerce.backend.model.request.user;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UpdateUserAddressRequestTest {

    @ParameterizedTest
    @CsvFileSource(resources = "/Keyist-update_address-output.csv", numLinesToSkip = 1)
    public void parametrized_test_for_update_user_address_request(String city, String state, String zip, String country, String address, String expected) {
        UpdateUserAddressRequest updateUserAddressRequest = new UpdateUserAddressRequest();

        updateUserAddressRequest.setCity(city);
        updateUserAddressRequest.setState(state);
        updateUserAddressRequest.setZip(zip);
        updateUserAddressRequest.setCountry(country);
        updateUserAddressRequest.setAddress(address);

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<UpdateUserAddressRequest>> violations = validator.validate(updateUserAddressRequest);

        String actual;

        if (violations.size() > 0) {
            actual = "exception";
        } else {
            actual = "success";
        }

        assertEquals(actual, expected);
    }
}
