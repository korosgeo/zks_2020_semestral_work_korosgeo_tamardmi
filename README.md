# ZKS semestral work
###### by Georgii Korostii (korosgeo), Dmitriy Tamarkov (tamardmi)

Current project contains tests for project that can be found [here](https://github.com/antkaynak/Keyist-Ecommerce).
All the back-end unit tests created are located in resource_server module.

Information about files in this project:

1. Keyist-Ecommerce - the project for which tests were created.
2. assignment - directory that contains results obtained in the process of fulfiling semestral work assignment.